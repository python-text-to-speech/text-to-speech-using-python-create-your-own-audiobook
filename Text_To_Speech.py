# Make sure you have gtts installed. To install write in the terminal "pip install gtts"
from gtts import gTTS

import os
# Opening and reading the files
f = open('test.txt')
x = f.read()
#Specifiying the language for the speech output
language = 'en'
# Defaullt speech settings
audio = gTTS(text=x, lang=language, slow=False) # the text file is x, the lang variable is the language and the slow variable tells if the speech should be slow or fast
# Creating and saving the .wav speech file.
audio.save("test.wav") # The audio file is saved by this name. You can add any name eg: MyAudio.wav
os.system("test.wav")
# If you wish to make any cjhanges to my code then please make a pull request for it.
